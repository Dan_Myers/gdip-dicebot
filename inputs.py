from machine import Pin

class Button:
    def __init__(self, gpioPin:int) -> None:
        """Initialise Button Object with internal pull up resistor

        :param int gpioPin: GPIO pin number
        """
        self.button = Pin(gpioPin, Pin.IN, Pin.PULL_UP)
        self.beenPressed = False
    
    def state(self) -> bool:
        """Check button state

        :return Boolean: Is button pressed
        """
        isPressed = False
        if not self.button.value():
            isPressed  = True
        
        return isPressed

    def onRelease(self) -> bool:
        """Actuate on button release

        :return Boolean: Has button been released after pressed
        """
        beenReleased = False
        if self.state():
            self.beenPressed = True
        else:
            if self.beenPressed:
                self.beenPressed = False
                beenReleased = True

        return beenReleased

class Switch:
    def __init__(self, gpioPin:int) -> None:
        """Initialise Switch Object

        :param int gpioPin: GPIO pin number
        """
        self.switch = Pin(gpioPin, Pin.IN, Pin.PULL_UP)
    
    def state(self) -> bool:
        """Check Switch state

        :return Boolean: Is switch active
        """
        isActive = False
        if self.switch.value():
            isActive = True
        
        return isActive