# Dicebot

Dicebot is a product that will automatically roll a die in a randomised way. The primary application for this is to roll a die in a fair and automatic way, when playing board games, or other activities that involve rolling dice, as it will eliminate the possibility of cheating or general human error (e.g. throwing dice off of a table).

## Prerequisites

* Raspberry Pi Pico
* Lynxmotion AL5D robot arm
