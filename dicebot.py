import time, json
from outputs import LED, Servo
from inputs import Button, Switch

class DiceBot:
    def __init__(self) -> None:
        """Inidialise main DiceBot Object
        """
        self.initialisePins()
        self.state = "MANUAL"
        self.recording = False
        self.playBackStep = 0
        self.home = [4915, 5599, 6499, 5203, 1963]
        self.moveHome()

    def initialisePins(self) -> None:
        """Initialise all GPIO pins and their respective IO devices
        """
        self.TestLED      = LED(25)
        self.MovingLED    = LED(7)
        self.RecordingLED = LED(6)
        self.ModeLED      = LED(5)

        self.TestLED.off()
        self.MovingLED.off()
        self.RecordingLED.off()
        self.ModeLED.off()

        self.Base     = Servo(4)
        self.Shoulder = Servo(3)
        self.Elbow    = Servo(2)
        self.Wrist    = Servo(1)
        self.Gripper  = Servo(0)

        self.BaseButton     = Button(15)
        self.ShoulderButton = Button(14)
        self.ElbowButton    = Button(13)
        self.WristButton    = Button(12)
        self.GripperButton  = Button(11)
        self.RecordButton   = Button(10)

        self.Direction   = Switch(9)
        self.ControlMode = Switch(8)

    def mainLoop(self) -> None:
        """Main loop of system
        """
        while True:
            time.sleep_ms(20)
            # self.debugIO()
            self.stateTransitions()

    def checkControls(self) -> None:
        """Check the inputs then perform their respective actions
        """
        if self.BaseButton.state() is True:
            if self.Direction.state() is True:
                self.Base.rotateAngle(1, True)
            else:
                self.Base.rotateAngle(-1, True)

        if self.ShoulderButton.state() is True:
            if self.Direction.state() is True:
                self.Shoulder.rotateAngle(1, True)
            else:
                self.Shoulder.rotateAngle(-1, True)

        if self.ElbowButton.state() is True:
            if self.Direction.state() is True:
                self.Elbow.rotateAngle(1, True)
            else:
                self.Elbow.rotateAngle(-1, True)

        if self.WristButton.state() is True:
            if self.Direction.state() is True:
                self.Wrist.rotateAngle(1, True)
            else:
                self.Wrist.rotateAngle(-1, True)

        if self.GripperButton.state() is True:
            if self.Direction.state() is True:
                self.Gripper.rotateAngle(1, True)
            else:
                self.Gripper.rotateAngle(-1, True)

        if self.BaseButton.state() or self.ShoulderButton.state() or \
        self.ElbowButton.state() or self.WristButton.state() or self.GripperButton.state():
            self.MovingLED.on()
        else:
            self.MovingLED.off()

    def debugIO(self) -> None:
        """Test the inputs of the system by printing to debug console
        """
        if self.BaseButton.state() is True:
            print("Base Button")

        if self.ShoulderButton.state() is True:
            print("Shoulder Button")

        if self.ElbowButton.state() is True:
            print("Elbow Button")

        if self.WristButton.state() is True:
            print("Wrist Button")

        if self.GripperButton.state() is True:
            print("Gripper Button")

        if self.RecordButton.state() is True:
            print("Record Button")

        if self.Direction.state() is True:
            print("Direction Switch")

        if self.ControlMode.state() is True:
            print("ControlMode Switch")

    def stateTransitions(self) -> None:
        """Manage state transistions of the system, including LED for mode changes
        """
        print(self.state)
        if self.state == "MANUAL":
            self.checkControls()
            if self.ControlMode.state() is False:
                self.ModeLED.on()
                self.state = "AUTOMATIC"

            if self.RecordButton.onRelease():
                self.RecordingLED.on()
                self.state = "RECORDING"

        elif self.state == "RECORDING":
            self.recordingMovement()
            self.checkControls()
            if self.RecordButton.onRelease():
                self.RecordingLED.off()
                self.recording = False
                self.state = "MANUAL"

        elif self.state == "AUTOMATIC":
            if self.ControlMode.state() is True:
                self.ModeLED.off()
                self.state = "MANUAL"

            if self.RecordButton.onRelease():
                self.playBackStep=0
                self.state = "PLAYBACK"

        elif self.state == "PLAYBACK":
            self.playbackMovement()
            if self.RecordButton.onRelease():
                self.state = "AUTOMATIC"

    def recordingMovement(self) -> None:
        """Append current position of system to json file. Create empty file first execution
        """
        if self.recording is False:
            with open('recording.json', 'w') as f:
                data = {}
                data["Base"]=[]
                data["Shoulder"]=[]
                data["Elbow"]=[]
                data["Wrist"]=[]
                data["Gripper"]=[]
                json.dump(data, f)

            self.recording = True

        with open('recording.json', 'r') as f:
            data = json.load(f)

        with open('recording.json', 'w') as f:
            data["Base"].append(self.Base.duty)
            data["Shoulder"].append(self.Shoulder.duty)
            data["Elbow"].append(self.Elbow.duty)
            data["Wrist"].append(self.Wrist.duty)
            data["Gripper"].append(self.Gripper.duty)
            json.dump(data, f)

    def playbackMovement(self) -> None:
        """Read json file and playback movement in order from recorded path
        """
        with open('recording.json', 'r') as f:
            data = json.load(f)
            if self.playBackStep > len(data["Base"])-1:
                self.playBackStep=0

            self.Base.rotateValue(data["Base"][self.playBackStep], False)
            self.Shoulder.rotateValue(data["Shoulder"][self.playBackStep], False)
            self.Elbow.rotateValue(data["Elbow"][self.playBackStep], False)
            self.Wrist.rotateValue(data["Wrist"][self.playBackStep], False)
            self.Gripper.rotateValue(data["Gripper"][self.playBackStep], False)

            self.playBackStep+=1

    def moveHome(self) -> None:
        """Move all motors to their respective home position
        """
        self.Base.rotateValue(self.home[0], False)
        self.Shoulder.rotateValue(self.home[1], False)
        self.Elbow.rotateValue(self.home[2], False)
        self.Wrist.rotateValue(self.home[3], False)
        self.Gripper.rotateValue(self.home[4], False)