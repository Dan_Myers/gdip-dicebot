from machine import Pin, PWM

class LED:
    def __init__(self, gpioPin:int) -> None:
        """Initialise LED Object

        :param int gpioPin: GPIO pin number
        """
        self.led = Pin(gpioPin, Pin.OUT)
        self.led.off()

    def toggle(self) -> None:
        """Toggle LED state
        """
        self.led.toggle()

    def off(self) -> None:
        """Turn LED off
        """
        self.led.off()
    
    def on(self) -> None:
        """Turn LED on
        """
        self.led.on()

class Servo:
    def __init__(self, gpioPin:int) -> None:
        """Initialise Servo Object

        :param int gpioPin: GPIO pin number
        """
        self.servo = PWM(Pin(gpioPin))
        self.servo.freq(50)
        self.MIN = 1638
        self.MAX = 8192
        self.duty = 0

    def rotateAngle(self, angle:float, relative:bool) -> None:
        """Rotate the servo to passed angle

        :param float angle: Angle to move servo to (0<=final angle<=180)
        :param bool relative: True for relative, False for absolute
        """
        duty = int((angle/180)*(self.MAX-self.MIN))

        if relative is True:
            self.duty += duty
        else:
            self.duty = duty + self.MIN

        self.setDuty()
    
    def rotateValue(self, value:int, relative:bool) -> None:
        """Rotate the servo to passed value

        :param int value: Value to use for duty cycle (1638<=final value<=8192)
        :param bool relative: True for relative, False for absolute
        """
        if relative is True:
            self.duty += value
        else:
            self.duty = value

        self.setDuty()

    def setDuty(self) -> None:
        """Check validity of duty cycle and set value for the servo
        """
        if self.duty > self.MAX:
            self.duty = self.MAX
        elif self.duty < self.MIN:
            self.duty = self.MIN

        self.servo.duty_u16(self.duty)